# AaDS
Project contains applications, algorithms and solutions to problems described below


# Projects
## List 1

(Description in progress)

## List 2

(Description in progress) 

## List 3

(Description in progress) 

## List 4

(Description in progress) 

# Basic and external libraries

- openJDK-15,
- Jupyter: Junit 5.4 (for Lists: 1, 2, 3, 4),

# Author

- [Jakub Szwedowicz](https://github.com/JakubSzwedowicz)
